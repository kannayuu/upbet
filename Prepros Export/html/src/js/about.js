// vue

const vm = new Vue({
  el: "#mainContainer",
  mixins: [vueMixins],
  data: { pageData },
  mounted() {},
  components: {}
});

let galleryThumbs = new Swiper(".ab2_gallery-thumbs", {
  freeMode: true,
  slidesPerView: "auto",
  centeredSlides: true,
  loopedSlides: 4,
  slideToClickedSlide: true,
  loop: true,
  slidesPerView: 5,
  breakpoints: {
    768: {
      slidesPerView: 1
    }
  }
});

let galleryTop = new Swiper(".ab2_gallery-top", {
  loop: true,
  centeredSlides: true,
  loopedSlides: 4,
  slidesPerView: 3,
  breakpoints: {
    768: {
      slidesPerView: 1
    }
  },
  navigation: {
    nextEl: ".ab2-next",
    prevEl: ".ab2-prev"
  }
});

galleryTop.controller.control = galleryThumbs;
galleryThumbs.controller.control = galleryTop;

if (window.matchMedia("(max-width:1024px)").matches) {
  const swiper１ = new Swiper(".ab1_swiper-container", {
    slidesPerView: 1,
    navigation: {
      nextEl: ".ab1-next",
      prevEl: ".ab1-prev"
    }
  });
}
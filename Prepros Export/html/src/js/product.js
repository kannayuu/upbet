// vue
const vm = new Vue({
  el: "#mainContainer",
  mixins: [vueMixins],
  data: { pageData, filter: "選擇分類", show: 0 },
  mounted() {},

  computed: {
    selectData: {
      // get: function() {
      //   console.log(this.filter);
      //   const filter = this.filter;
      //   // 利用click去綁定 選擇分類跟value
      //   if (filter === "選擇分類") {
      //     return this.pageData.pro;
      //     // 如果是選擇分類show全部
      //   } else if (filter !== "選擇分類") {
      //     let result = this.pageData.pro.filter(function(item, index, arr) {
      //       return item.type == filter;
      //     });
      //     //秀個別
      //     return result;
      //   }
      // }
    }
  }
});

////下拉選單用
// $(".ds_placeholder").on("click", function(e) {
//   e.preventDefault();
//   if ($(".ds_select").hasClass("open")) $(".ds_select").removeClass("open");
//   else $(".ds_select").addClass("open");
// });

// $(".ds_list a").on("click", function(e) {
//   e.preventDefault();
//   $(".ds_placeholder").text($(this).text());
//   $(".ds_select").removeClass("open");
// });

if (window.matchMedia("(max-width:1024px)").matches) {
  const swiper = new Swiper(".pro__se5-swiper-container", {
    slidesPerView: 1,
    navigation: {
      nextEl: ".pro__se5-next",
      prevEl: ".pro__se5--prev"
    },
    loop: true
  });
}

//開場動畫設定
let proAniOpen = function() {
  let proOpen = new TimelineLite();
  proOpen
    .from(".product__title", 0.5, {
      //最新消息
      delay: 0.3,
      y: 30,
      opacity: 0
    })
    .from(".title", 0.5, {
      //最新消息
      delay: 0.3,
      y: 30,
      opacity: 0
    })
    .from(".info", 0.5, {
      //最新消息
      delay: 0.3,
      y: 30,
      opacity: 0
    });
};
proAniOpen();

//=========================================捲軸區域
//=========================================捲軸區域
//捲軸事件;
let controller = new ScrollMagic.Controller({
  addIndicators: true
});
//第２屏幕動畫
let proAni = new TimelineLite();
proAni.from(".txt__show", 0.5, {
  //最新消息
  delay: 0.3,
  y: 30,
  opacity: 0
});
let proScroll = new ScrollMagic.Scene({
    triggerElement: ".pro__se2", //觸發點
    triggerHook: 0.75,
    reverse: !1
  })
  .setTween(proAni)
  .addTo(controller);

//第３屏幕動畫
let proAni2 = new TimelineLite();
proAni2
  .from(".se3__title>.real__txt", 0.5, {
    //最新消息
    delay: 0.3,
    y: 30,
    opacity: 0
  })
  .staggerFrom(
    ".se3__card",
    1, {
      y: "+=30",
      opacity: 0,
      clearProps: "opacity, transform"
    },
    0.5
  );
let proScroll2 = new ScrollMagic.Scene({
    triggerElement: ".pro__se3", //觸發點
    triggerHook: 0.75,
    reverse: !1
  })
  .setTween(proAni2)
  .addTo(controller);

//第４屏幕動畫
let proAni3 = new TimelineLite();
proAni3
  .from("pro__se4txt>.title", 0.5, {
    //最新消息
    delay: 0.3,
    y: 30,
    opacity: 0
  })
  .from("info>.items", 1, {
    delay: 0.3,
    y: 30,
    opacity: 0
  });
let proScroll3 = new ScrollMagic.Scene({
    triggerElement: ".pro__se4", //觸發點
    triggerHook: 0.75,
    reverse: !1
  })
  .setTween(proAni3)
  .addTo(controller);

//第５屏幕動畫
let proAni4 = new TimelineLite();
proAni4.staggerFrom(
  ".slide__card",
  1, {
    y: "+=30",
    opacity: 0,
    clearProps: "opacity, transform"
  },
  0.5
);

let proScroll4 = new ScrollMagic.Scene({
    triggerElement: ".pro__se5", //觸發點
    triggerHook: 0.75,
    reverse: !1
  })
  .setTween(proAni4)
  .addTo(controller);
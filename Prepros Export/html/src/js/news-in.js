// vue
const vm = new Vue({
  el: "#mainContainer",
  mixins: [vueMixins],
  data: { pageData },
  mounted() {}
});

let newsinAniOpen = function() {
  let newsinOpen = new TimelineLite();
  newsinOpen
    .from(".main__title", 0.5, {
      //news
      delay: 0.3,
      y: 30,
      opacity: 0
    })
    .from(".sec__title", 0.5, {
      //標題
      delay: 0.3,
      y: 30,
      opacity: 0
    })
    .from(".info__area", 0.5, {
      //下面區塊文章
      delay: 0.3,
      y: 30,
      opacity: 0
    });
};
newsinAniOpen();
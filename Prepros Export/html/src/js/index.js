// vue
const vm = new Vue({
  el: "#mainContainer",
  mixins: [vueMixins],
  data: { pageData },
  mounted() {
    // window.swiper2 = new Swiper(".ise2_swiper-container", {
    //   slidesPerView: 4,
    //   pagination: {
    //     type: "fraction"
    //   },
    //   navigation: {
    //     nextEl: ".ise2-next",
    //     prevEl: ".ise2-prev"
    //   },
    //   breakpoints: {
    //     767: {
    //       slidesPerView: 1
    //     },
    //     1200: {
    //       slidesPerView: 2
    //     }
    //   }
    // });
  }
});
//箭頭標準版
// let swiperise2 = new Swiper(".ise2-container", {
//   navigation: {
//     nextEl: ".ise2-next",
//     prevEl: ".ise2-prev"
//   }
// });

const s1slide = function() {
  let $img = $(".main__bgc");
  TweenMax.set($img, { autoAlpha: 0 });
  TweenMax.to($img, 3, { autoAlpha: 1 });
  const tl = new TimelineMax({});
  tl.from(".people", 1, {
    opacity: 0,
    x: -100,
    clearProps: "all"
  }).from(".ball", 1, {
    opacity: 0,
    x: -100,
    clearProps: "all"
  });
};

const swiper1 = new Swiper(".ise1_swiper-container", {
  pagination: {
    el: ".ise1_swiper-pagination",
    clickable: true
  },
  navigation: {
    nextEl: ".ise1-next",
    prevEl: ".ise1-prev"
  },
  effect: "fade",
  fadeEffect: {
    crossFade: true
  },
  on: {
    slideChange: s1slide
  }
  // autoplay: {
  //   delay: 6000,
  //   disableOnInteraction: false,
  //   waitForTransition: false
  // }
});
//桌機判定
// if (window.matchMedia(bkpDsk).matches) {
//   console.log("桌機");
// }
//手機啟動
if (window.matchMedia("(max-width:1024px)").matches) {
  const swiper2 = new Swiper(".ise2_swiper-container", {
    slidesPerView: 2,
    navigation: {
      nextEl: ".ise2-next",
      prevEl: ".ise2-prev"
    },
    loop: true,
    breakpoints: {
      767: {
        slidesPerView: 1
      }
    }
  });

  const swiper4 = new Swiper(".ise4_swiper-container", {
    slidesPerView: 2,
    pagination: {
      el: ".ise4_swiper-pagination",
      clickable: true
    },
    loop: true,
    breakpoints: {
      767: {
        slidesPerView: 1
      }
    }
  });
}

// 開場
if (window.matchMedia(bkpDsk).matches) {
  window.addEventListener("load", () => {
    s1slide();
  });
} else {
  document.addEventListener("DOMContentLoaded", () => {
    // console.log('DOMContentLoaded');
    // searchAni();
  });
}
//第三個區域滾動式差
let img = document.querySelector(".dsk-2");
new simpleParallax(img);

//news 移動到第二個畫面用區
let topSe2 = document.querySelector("#gotoSe2");
topSe2.addEventListener(
  "click",
  () => {
    const target = $(".index__se2");
    console.log(target);
    //選到第二個篇幅
    TweenLite.to(window, 1, {
      scrollTo: { y: target, autoKill: false, offsetY: 300 },
      ease: Power4.easeOut
    });
  },
  false
);
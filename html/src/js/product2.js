// vue
const vm = new Vue({
  el: "#mainContainer",
  mixins: [vueMixins],
  data: { pageData, filter: "選擇分類", show: 0 },
  mounted() {},

  computed: {
    selectData: {
      // get: function() {
      //   console.log(this.filter);
      //   const filter = this.filter;
      //   // 利用click去綁定 選擇分類跟value
      //   if (filter === "選擇分類") {
      //     return this.pageData.pro;
      //     // 如果是選擇分類show全部
      //   } else if (filter !== "選擇分類") {
      //     let result = this.pageData.pro.filter(function(item, index, arr) {
      //       return item.type == filter;
      //     });
      //     //秀個別
      //     return result;
      //   }
      // }
    }
  }
});

const test = function() {
  let $img = $(".main__img");
  TweenMax.set($img, {
    delay: 1,
    x: -30
  });
  TweenMax.to($img, 1, { x: 0 });
};
test;

////下拉選單用
// $(".ds_placeholder").on("click", function(e) {
//   e.preventDefault();
//   if ($(".ds_select").hasClass("open")) $(".ds_select").removeClass("open");
//   else $(".ds_select").addClass("open");
// });

// $(".ds_list a").on("click", function(e) {
//   e.preventDefault();
//   $(".ds_placeholder").text($(this).text());
//   $(".ds_select").removeClass("open");
// });

if (window.matchMedia("(max-width:1024px)").matches) {
  const swiper = new Swiper(".pro__se5-swiper-container", {
    slidesPerView: 1,
    navigation: {
      nextEl: ".pro__se5-next",
      prevEl: ".pro__se5-prev"
    }
    // loop: true
  });
}

//開場動畫設定

let proAniOpen = function() {
  let proOpen = new TimelineLite();
  proOpen
    .from(".real__img", 1, {
      delay: 4,
      x: 200,
      scale: 0.9,
      ease: Power2.easeOut
    })
    .from(".pro__shadow", 1, {
      x: 10,
      ease: Power2.easeOut
    })
    .from(".product__title", 0.5, {
      //最新消息
      delay: 0.5,
      y: 30,
      opacity: 0,
      ease: Power2.easeOut
    })
    .from(".title", 0.5, {
      //最新消息
      delay: -0.3,
      y: 30,
      opacity: 0,
      ease: Power2.easeOut
    })
    .from(".info", 0.5, {
      //最新消息
      delay: -0.3,
      y: 30,
      opacity: 0,
      ease: Power2.easeOut
    });
};
proAniOpen();

// .from(".mask > .txt__show > .title", 0.5, {
//   delay: 2,
//   y: 30,
//   opacity: 0,
//   ease: Power2.easeOut
// })
// .from(".mask > .txt__show > .info", 0.5, {
//   delay: 0.3,
//   y: 30,
//   opacity: 0,
//   ease: Power2.easeOut
// });

//product 移動到第二個畫面用區
//product 移動到第二個畫面用區
//product 移動到第二個畫面用區
let topSe2 = document.querySelector("#gotoSe2");
topSe2.addEventListener(
  "click",
  () => {
    const target = $(".pro__se2");
    console.log(target);
    //選到第二個篇幅
    TweenLite.to(window, 1, {
      scrollTo: { y: target, autoKill: false, offsetY: 300 },
      ease: Power4.easeOut
    });
  },
  false
);
//第二個區域滾動式差
//第二個區域滾動式差
//第二個區域滾動式差
let img = document.querySelector(".pro__se2>.bgc");
new simpleParallax(img);

let controller = new ScrollMagic.Controller({
  //addIndicators: true
});

let proAni1 = new TimelineLite();
proAni1
  .from(".s2__title", 0.5, {
    delay: 0.3,
    y: 30,
    opacity: 0,
    ease: Power2.easeOut
  })
  .from(".s2__info", 0.5, {
    delay: -0.3,
    y: 30,
    opacity: 0,
    ease: Power2.easeOut
  });
//indexAni1;
let proScroll = new ScrollMagic.Scene({
    triggerElement: ".pro__se2", //觸發點
    triggerHook: 0.75,
    reverse: !1
  })
  .setTween(proAni1)
  .addTo(controller);

///===============================
///===============================
///===============================
let proAni2 = new TimelineLite();
proAni2
  .from(".se3__title", 0.5, {
    delay: 0.3,
    y: 30,
    opacity: 0,
    ease: Power2.easeOut
  })
  .staggerFrom(
    ".se3__card",
    1, {
      y: "+=80",
      opacity: 0,
      clearProps: "opacity, transform"
    },
    0.5
  );
//indexAni1;
let proScroll2 = new ScrollMagic.Scene({
    triggerElement: ".pro__se3", //觸發點
    triggerHook: 0.7,
    reverse: !1
  })
  .setTween(proAni2)
  .addTo(controller);

///===============================
///===============================
///===============================
let proAni3 = new TimelineLite();
proAni3
  .from(".main__bgc2", 0.5, {
    delay: 0.3,
    x: -200,
    opacity: 0,
    ease: Power2.easeOut
  })
  .from(".product__titles4", 0.5, {
    delay: -0.3,
    y: 30,
    opacity: 0,
    ease: Power2.easeOut
  })
  .from(".p4__info", 0.5, {
    delay: 0.3,
    y: 30,
    opacity: 0,
    ease: Power2.easeOut
  })
  .from(".p4__infotitle", 0.5, {
    delay: -0.3,
    y: 30,
    opacity: 0,
    ease: Power2.easeOut
  })
  .staggerFrom(
    ".info__item",
    0.5, {
      y: "+=30",
      opacity: 0,
      clearProps: "opacity, transform"
    },
    0.2
  )
  .from(".pro__shadow4", 0.5, {
    delay: 0.3,
    opacity: 0,
    ease: Power2.easeOut
  });
//indexAni1;
let proScroll3 = new ScrollMagic.Scene({
    triggerElement: ".pro__se4", //觸發點
    triggerHook: 0.72,
    reverse: !1
  })
  .setTween(proAni3)
  .addTo(controller);

///===============================
///===============================
///===============================
let proAni4 = new TimelineLite();
proAni4.staggerFrom(
  ".pros5slide__card",
  0.5, {
    y: "+=30",
    opacity: 0,
    clearProps: "opacity, transform"
  },
  0.3
);
//indexAni1;
let proScroll4 = new ScrollMagic.Scene({
    triggerElement: ".pro__se5", //觸發點
    triggerHook: 0.72,
    reverse: !1
  })
  .setTween(proAni4)
  .addTo(controller);
// vue
const vm = new Vue({
  el: "#mainContainer",
  mixins: [vueMixins],
  data: { pageData },
  mounted() {}
});

let newsinAniOpen = function() {
  let newsinOpen = new TimelineLite();
  newsinOpen
    .from(".real__img", 0.5, {
      delay: 5,
      x: -200,
      opacity: 0,
      ease: Power2.easeOut
    })
    .from(".main__title", 0.5, {
      //news
      delay: 0.3,
      y: 30,
      opacity: 0,
      ease: Power2.easeOut
    })
    .from(".sec__title", 0.5, {
      //標題
      delay: -0.3,
      y: 30,
      opacity: 0,
      ease: Power2.easeOut
    })
    .from(".info__area", 0.5, {
      //下面區塊文章
      delay: -0.3,
      y: 30,
      opacity: 0,
      ease: Power2.easeOut
    });
};
newsinAniOpen();

// let bgcMove = new TimelineMax({});
// bgcMove.to(".contact__se1bgc", 3, {
//   backgroundPosition: "0px center",
//   ease: Linear.easeNone
// });
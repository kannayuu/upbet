// vue

const vm = new Vue({
  el: "#mainContainer",
  mixins: [vueMixins],
  data: { pageData },
  mounted() {}
});

// ===================================表單AJAX區域
// ===================================表單AJAX區域
// ===================================表單AJAX區域

$("#register_form")
  .validator()
  .on("submit", function(e) {
    if (e.isDefaultPrevented()) {
      // 未驗證通過 則不處理
      // console.log("沒有送出");
      return;
    } else {
      // 通過送出表單
      console.log($("#register_form").serialize());
      $.ajax({
        type: "POST",
        url: "http://localhost:3000/test",
        data: $("#register_form").serialize(),
        dataType: "json",
        success: function success(response) {
          alert("感謝你的填寫，我們會儘素聯繫你");
        }
      }).done(function(data) {
        $("#register_form")
          .find(":text,textarea")
          .each(function() {
            $(this).val("");
          });
        $("#inputEmail").val("");
      });
    }
    e.preventDefault(); // 防止原始 form 提交表單
  });

///contact================================動畫
///contact================================動畫
let conAni = function() {
  let aboSe1Ani = new TimelineLite();
  aboSe1Ani
    .from(".contact__title", 0.5, {
      delay: 5,
      y: 30,
      opacity: 0,
      ease: Power2.easeOut
    })
    .from(".title", 0.5, {
      delay: -0.3,
      y: 30,
      opacity: 0,
      ease: Power2.easeOut
    })
    .from(".info", 0.5, {
      //標題
      delay: -0.3,
      y: 30,
      opacity: 0,
      ease: Power2.easeOut
    });
};
conAni();

$("#upTop").hide();
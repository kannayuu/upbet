// vue

const vm = new Vue({
  el: "#mainContainer",
  mixins: [vueMixins],
  data: { pageData },
  mounted() {},
  components: {}
});

let galleryThumbs = new Swiper(".ab2_gallery-thumbs", {
  freeMode: true,
  slidesPerView: "auto",
  centeredSlides: true,
  loopedSlides: 4,
  slideToClickedSlide: true,
  loop: true,
  slidesPerView: 5,
  breakpoints: {
    768: {
      slidesPerView: 1
    }
  }
});

let galleryTop = new Swiper(".ab2_gallery-top", {
  loop: true,
  speed: 1000,
  centeredSlides: true,
  loopedSlides: 4,
  slidesPerView: 3,
  breakpoints: {
    768: {
      slidesPerView: 1
    }
  },
  navigation: {
    nextEl: ".ab2-next",
    prevEl: ".ab2-prev"
  },
  autoplay: {
    delay: 3000,
    disableOnInteraction: false,
    waitForTransition: false
  },
  speed: 1500
});

galleryTop.controller.control = galleryThumbs;
galleryThumbs.controller.control = galleryTop;

if (window.matchMedia("(max-width:1024px)").matches) {
  const swiper１ = new Swiper(".ab1_swiper-container", {
    navigation: {
      nextEl: ".ab1-next",
      prevEl: ".ab1-prev"
    }
  });
}

//about 移動到第二個畫面用區
let topSe2 = document.querySelector("#gotoSe2");
topSe2.addEventListener(
  "click",
  () => {
    const target = $(".ab__se2");
    //console.log(target);
    //選到第二個篇幅
    TweenLite.to(window, 1, {
      scrollTo: { y: target, autoKill: false, offsetY: 300 },
      ease: Power4.easeOut
    });
  },
  false
);
//骰子動畫
new TweenMax.to(".dice", 1, {
  delay: 5,
  scaleX: "1",
  scaleY: "1",
  transformOrigin: "0% 0%",
  ease: Power2.easeOut
});

let aboAni = new TimelineLite();
aboAni
  .from(".dice", 0.5, {
    delay: 4
  })
  .from(".real__img", 1, {
    delay: 1,
    x: 200,
    opacity: 0,
    ease: Power2.easeOut
  })
  .from(".main__title", 0.5, {
    delay: 0.5,
    y: 30,
    opacity: 0,
    ease: Power2.easeOut
  })
  .from(".cn__title", 0.5, {
    delay: -0.3,
    y: 30,
    opacity: 0,
    ease: Power2.easeOut
  })
  .from(".info", 0.5, {
    delay: -0.3,
    y: 30,
    opacity: 0,
    ease: Power2.easeOut
  });

//=========================================捲軸區域
//=========================================捲軸區域
//捲軸事件;
let controller = new ScrollMagic.Controller({
  //addIndicators: true
});

let aboAni2 = new TimelineLite();
aboAni2.staggerFrom(
  ".ab__card",
  1, {
    y: "+=30",
    opacity: 0,
    clearProps: "opacity, transform"
  },
  0.5
);

window.addEventListener("load", () => {});

// 開場
if (window.matchMedia(bkpDsk).matches) {
  //桌機啟動
  let controller = new ScrollMagic.Controller({
    //addIndicators: true
  });
  let aboScroll1 = new ScrollMagic.Scene({
      triggerElement: ".ab__se2", //觸發點
      triggerHook: 0.75,
      reverse: !1
    })
    .setTween(aboAni2)
    .addTo(controller);

  window.addEventListener("load", () => {
    aboAni;
  });
} else {
  document.addEventListener("DOMContentLoaded", () => {});
}
//背景圖動畫
let bgcMove = new TimelineMax({});
bgcMove.to(".main__img", 3, {
  backgroundPosition: "-60px center",
  ease: Linear.easeNone
});
// vue
const vm = new Vue({
  el: "#mainContainer",
  mixins: [vueMixins],
  data: { pageData, filter: "選擇分類", show: 0 },
  mounted() {},
  methods: {},
  computed: {
    selectData: {
      // get: function() {
      //   console.log(this.filter);
      //   const filter = this.filter;
      //   // 利用click去綁定 選擇分類跟value
      //   if (filter === "選擇分類") {
      //     return this.pageData.pro;
      //     // 如果是選擇分類show全部
      //   } else if (filter !== "選擇分類") {
      //     let result = this.pageData.pro.filter(function(item, index, arr) {
      //       return item.type == filter;
      //     });
      //     //秀個別
      //     return result;
      //   }
      // }
    }
  }
});
const swiperser = new Swiper(".ser_swiper-container", {
  pagination: {
    el: ".ser-pagination",
    type: "progressbar"
  },
  navigation: {
    nextEl: ".ser-next",
    prevEl: ".ser-prev"
  },
  speed: 1000
});

const swiperser2 = new Swiper(".ser2_swiper-container", {
  pagination: {
    el: ".ser2-pagination",
    type: "fraction"
  },
  renderFraction: function(currentClass, totalClass) {
    return (
      '<span class="' +
      currentClass +
      '"></span>' +
      '<span class="' +
      totalClass +
      '"></span>'
    );
  },
  navigation: {
    nextEl: ".ser-next",
    prevEl: ".ser-prev"
  },
  speed: 1000
});
//services 移動到第二個畫面用區
let topSe2 = document.querySelector("#gotoSe2");
topSe2.addEventListener(
  "click",
  () => {
    const target = $(".ser__se2");
    console.log(target);
    //選到第二個篇幅
    TweenLite.to(window, 1, {
      scrollTo: { y: target, autoKill: false, offsetY: 300 },
      ease: Power4.easeOut
    });
  },
  false
);

//開場動畫設定
let proAniOpen = function() {
  let proOpen = new TimelineLite();
  proOpen
    .from(".real__img", 0.5, {
      delay: 4,
      x: 200,
      opacity: 0,
      ease: Power2.easeOut
    })
    .from(".service__title", 0.5, {
      //最新消息
      delay: 0.3,
      y: 30,
      opacity: 0,
      ease: Power2.easeOut
    })
    .from(".title", 0.5, {
      //最新消息
      delay: -0.3,
      y: 30,
      opacity: 0,
      ease: Power2.easeOut
    })
    .from(".info", 0.5, {
      //最新消息
      delay: -0.3,
      y: 30,
      opacity: 0,
      ease: Power2.easeOut
    });
};
proAniOpen();
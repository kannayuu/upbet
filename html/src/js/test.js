// vue
const vm = new Vue({
  el: "#mainContainer",
  mixins: [vueMixins],
  data: { pageData }
});

let people = [...document.querySelectorAll(".ise1__people")];
let ball = [...document.querySelectorAll(".ise1__ball")];

const swiper1 = new Swiper(".ise1_swiper-container", {
  pagination: {
    el: ".ise1_swiper-pagination",
    clickable: true
  },
  navigation: {
    nextEl: ".ise1-next",
    prevEl: ".ise1-prev"
  },
  effect: "fade",
  fadeEffect: {
    crossFade: true
  },
  on: {
    slideChange: function() {
      let active = this.activeIndex;
      let peopleImg = people[active];
      let ballImg = ball[active];
      let peopleBall = new TimelineLite();
      peopleBall
        .from(peopleImg, 0.5, {
          delay: 0.5,
          scale: 0,
          transformOrigin: "0% 50%",
          ease: Bounce.easeOut
        })
        .from(ballImg, 0.5, {
          delay: 0.5,
          scale: 0,
          transformOrigin: "0% 50%",
          ease: Bounce.easeOut
        });
    }
  },
  autoplay: {
    delay: 6000,
    disableOnInteraction: false,
    waitForTransition: false
  }
});
//桌機判定
// if (window.matchMedia(bkpDsk).matches) {
//   console.log("桌機");
// }
//手機啟動
// let index = swiper1.activeIndex;
// swiper1.on("slideChange", () => {
//   console.log(index);
// });

const s2slide = function() {
  const t2 = new TimelineMax();
  t2.from(".img__title", 1, {
    opacity: 0,
    y: 10,
    clearProps: "all"
  });
};

if (window.matchMedia("(max-width:1024px)").matches) {
  const swiper2 = new Swiper(".ise2_swiper-container", {
    navigation: {
      nextEl: ".ise2-next",
      prevEl: ".ise2-prev"
    },
    on: {
      slideChange: s2slide
    }
  });
}

const s3slide = function() {
  const t3 = new TimelineMax({});
  t3.from(".data__show", 0.5, {
      opacity: 0,
      y: 10,
      clearProps: "all"
    })
    .from(".info__show>.title", 0.5, {
      opacity: 0,
      y: 10,
      clearProps: "all"
    })
    .from(".info__show>.info", 0.5, {
      opacity: 0,
      y: 10,
      clearProps: "all"
    });
};

if (window.matchMedia("(max-width:767px)").matches) {
  const swiper4 = new Swiper(".ise4_swiper-container", {
    slidesPerView: 1,
    pagination: {
      el: ".ise4_swiper-pagination",
      clickable: true
    },
    loop: true,
    on: {
      slideChange: s3slide
    }
  });
}

if (window.matchMedia(bkpDsk).matches) {
  let img = document.querySelector(".se3__dsk");
  new simpleParallax(img, {
    scale: 1.5
  });
} else {
  let imgmbl = document.querySelector(".se3__mbl");
  new simpleParallax(imgmbl, {
    scale: 1.5
  });
}

//news 移動到第二個畫面用區
let topSe2 = document.querySelector("#gotoSe2");
topSe2.addEventListener(
  "click",
  () => {
    const target = $(".index__se2");
    console.log(target);
    //選到第二個篇幅
    TweenLite.to(window, 1, {
      scrollTo: { y: target, autoKill: false, offsetY: 300 },
      ease: Power4.easeOut
    });
  },
  false
);
//=========================================捲軸區域
//=========================================捲軸區域
//捲軸事件;
let controller = new ScrollMagic.Controller({
  //addIndicators: true
});

let indexAni1 = new TimelineLite();
indexAni1
  .staggerFrom(
    ".ise2__card",
    0.5, {
      y: "-=30",
      opacity: 0,
      clearProps: "opacity, transform",
      rotation: -20
    },
    0.2
  )
  .from(".ise2__items", 0.5, {
    delay: 0.3,
    y: 30,
    opacity: 0
  });

//只能桌機目前還沒處理
// indexAni1;
let index1Scroll = new ScrollMagic.Scene({
  triggerElement: ".index__se2", //觸發點
  triggerHook: 0.75,
  reverse: !1
}).setTween(indexAni1);

//下面這邊動畫有問題
// let indexAni3 = new TimelineLite();
// indexAni3.staggerFrom(
//   ".ise4__card",
//   1, {
//     y: "+=30",
//     opacity: 0,
//     clearProps: "opacity, transform"
//   },
//   0.5
// );
// indexAni3;

// let index2Scroll = new ScrollMagic.Scene({
//     triggerElement: ".index__se4", //觸發點
//     triggerHook: 0.75,
//     reverse: !1
//   })
//   .setTween(indexAni3)
//   .addTo(controller);

// function movePeopleBall() {
//  TweenMax.killAll();
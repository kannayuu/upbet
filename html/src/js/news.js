// vue
const vm = new Vue({
  el: "#mainContainer",
  mixins: [vueMixins],
  data: { pageData, filter: "選擇分類", show: 0 },
  mounted() {},
  computed: {
    selectData: {
      get: function() {
        //先需告一個一個要找的選項
        //下面要列印出來資料綁定選到return哪個
        // console.log(this.filter);
        const filter = this.filter;
        // 利用click去綁定 選擇分類跟value
        if (filter === "選擇分類") {
          // console.log(this.pageData.se2);
          return this.pageData.se2;
          // 如果是選擇分類show全部
        } else if (filter !== "選擇分類") {
          let result = this.pageData.se2.filter(function(item, index, arr) {
            return item.type == filter;
          });
          //秀個別
          return result;
        }
      }
    }
  }
});
//news 移動到第二個畫面用區
let topSe2 = document.querySelector("#gotoSe2");
topSe2.addEventListener(
  "click",
  () => {
    const target = $(".news__se2");
    console.log(target);
    //選到第二個篇幅
    TweenLite.to(window, 1, {
      scrollTo: { y: target, autoKill: false, offsetY: 300 },
      ease: Power4.easeOut
    });
  },
  false
);

if (window.matchMedia("(min-width:1199px)").matches) {
  //=========================================捲軸區域
  //=========================================捲軸區域
  //捲軸事件;
  let controller = new ScrollMagic.Controller({
    // addIndicators: true
  });
  //卡片動畫
  let newsAni = new TimelineLite();
  newsAni.staggerFrom(
    ".news__card",
    0.5, {
      y: "+=30",
      opacity: 0,
      clearProps: "opacity, transform",
      ease: Power2.easeOut
    },
    0.5
  );
  let newsScroll = new ScrollMagic.Scene({
      triggerElement: ".news__se2", //觸發點
      triggerHook: 0.75,
      reverse: !1
    })
    .setTween(newsAni)
    .addTo(controller);
}

$(".ds_placeholder").on("click", function(e) {
  e.preventDefault();
  if ($(".ds_select").hasClass("open")) $(".ds_select").removeClass("open");
  else $(".ds_select").addClass("open");
});

$(".ds_list a").on("click", function(e) {
  e.preventDefault();
  $(".ds_placeholder").text($(this).text());
  $(".ds_select").removeClass("open");
});

//背景圖動畫

// const tese1 = function() {
//   let bgcMove = new TimelineMax({});
//   bgcMove.to(".news__img", 3, {
//     backgroundPosition: "0px center",
//     ease: Linear.easeNone
//   });
// };

let newsAni = new TimelineLite();
newsAni
  .from(".real__img", 0.5, {
    delay: 5,
    x: -200,
    opacity: 0,
    ease: Power2.easeOut
  })
  .from(".news__title", 0.5, {
    delay: 0.5,
    y: 30,
    opacity: 0,
    ease: Power2.easeOut
  })
  .from(".news__info", 0.5, {
    delay: -0.3,
    y: 30,
    opacity: 0,
    ease: Power2.easeOut
  })
  .from(".ds_select", 0.5, {
    delay: -0.3,
    opacity: 0,
    ease: Power2.easeOut
  });
const vm = new Vue({
  el: "#mainContainer",
  mixins: [vueMixins],
  data: { pageData },
  mounted() {},
  methods: {
    generateClassName(index) {
      return `ise2__txt-${index}`;
    }
  }
});

$(".main__greenLine").hide();

let people = [...document.querySelectorAll(".ise1__people")];
let ball = [...document.querySelectorAll(".ise1__ball")];
let mainBgc = [...document.querySelectorAll(".main__bgc>img")];
let typeInfo = [...document.querySelectorAll(".ise1__card>.main__txt")];
let typeTitle = [...document.querySelectorAll(".main__txt>.show__item")];
let title = [...document.querySelectorAll(".main__txt>.title")];
let info = [...document.querySelectorAll(".main__txt>.info")];
//swiper

const swiper1 = new Swiper(".ise1_swiper-container", {
  init: false,
  pagination: {
    el: ".ise1_swiper-pagination",
    clickable: true
  },
  navigation: {
    nextEl: ".ise1-next",
    prevEl: ".ise1-prev"
  },
  effect: "fade",
  fadeEffect: {
    crossFade: true
  },
  // autoplay: {
  //   delay: 6000
  // },
  speed: 1500
});

const firstAni = function() {
  let peopleImgFri = people[0];
  let ballImgFri = ball[0];
  let mainBgcFri = mainBgc[0];
  let tyinfoFri = typeInfo[0];
  let typeTitleFri = typeTitle[0];
  let titleFri = title[0];
  let infoFri = info[0];
  //console.log(mainBgcIndex);
  TweenMax.to(".mask-index", 3, {
    delay: 4,
    width: "0%",
    ease: Power2.easeOut
  });

  let peopleBall = new TimelineLite();

  peopleBall
    .from(".main__logo", 0.5, {
      delay: 4,
      opacity: 0
    })
    .from(".btn__language", 0.5, {
      opacity: 0,
      delay: -0.7
    })
    .from(".btn__area", 0.5, {
      opacity: 0,
      delay: -0.7
    })
    .from(".gotoSe2", 0.5, {
      opacity: 0,
      delay: -0.7
    })
    .from(mainBgcFri, 2, {
      delay: 0.5,
      x: -200,
      clearProps: "all",
      opacity: 0.8,
      scale: 1.1
    })
    .to(".go__3", 1, {
      delay: -1.8,
      autoAlpha: 1
    })
    .to(ballImgFri, 1, {
      delay: -1.8,
      autoAlpha: 1
    })
    .fromTo(
      ".go__3",
      1.3,
      {
        x: -500,
        y: 200
        // autoAlpha: 1
      },
      {
        delay: -0.9,
        x: -200,
        y: 100,
        autoAlpha: 0
      }
    )

    .set(peopleImgFri, {
      autoAlpha: 0
    })
    .fromTo(
      peopleImgFri,
      1,
      {
        x: -200,
        y: 100,
        autoAlpha: 0
      },
      {
        x: 0,
        y: 0,
        autoAlpha: 1
      }
    )
    .fromTo(
      ballImgFri,
      0.5,
      {
        x: -500,
        y: 500
      },
      {
        delay: -1,
        x: 0,
        y: 0,
        autoAlpha: 0
      }
    )
    .from(".ball__two", 1, {
      autoAlpha: 0,
      rotation: 360,
      repeat: 1,
      ease: Linear.easeInOut
    })
    .from(tyinfoFri, 0.5, {
      y: 30,
      opacity: 0
    })
    .from(".ise1-prev", 0.5, {
      x: 30,
      opacity: 0
    })
    .from(".ise1-next", 0.5, {
      x: -30,
      opacity: 0
    });

  // .to(ballImgFri, 0.5, {
  //   x: -50,
  //   opacity: 1,
  //   ease: Power2.easeOut,
  //   clearProps: "all"
  // })
};
firstAni();
window.addEventListener("load", () => {
  setInterval(() => {
    swiper1.init();
  }, 7000);
});

// =============================
// =============================
// =============================
// =============================
swiper1.on("slideChange", function() {
  $(".main__greenLine").hide();
  setTimeout(() => {
    $(".main__greenLine").show();
  }, 0);
  let active = this.activeIndex;
  let peopleImgIndex = people[active];
  let ballImgIndex = ball[active];
  let mainBgcIndex = mainBgc[active];
  let mainInfoIndex = typeInfo[active];
  //console.log(mainBgcIndex);
  let peopleBallIndex = new TimelineLite();
  peopleBallIndex
    .from(mainBgcIndex, 2, {
      x: -200,
      clearProps: "all",
      opacity: 0.8,
      scale: 1.1
    })
    .to(".go__3__2", 1, {
      delay: -1.8,
      autoAlpha: 1
    })
    .to(ballImgIndex, 1, {
      delay: -0.3,
      autoAlpha: 1
    })
    .fromTo(
      ".go__3__2",
      1.3,
      {
        x: -500,
        y: 200
        //autoAlpha: 1
      },
      {
        delay: -0.9,
        x: -200,
        y: 100,
        autoAlpha: 0
      }
    )
    .fromTo(
      peopleImgIndex,
      1,
      {
        autoAlpha: 0,
        x: -200,
        y: 100
      },
      {
        delay: -0.5,
        x: 0,
        y: 0,
        autoAlpha: 1
      }
    )
    .fromTo(
      ballImgIndex,
      0.5,
      {
        x: -500,
        y: 500
      },
      {
        delay: -1,
        x: 0,
        y: 0,
        autoAlpha: 0
      }
    )
    .from(".ball__two", 0.5, {
      autoAlpha: 0,
      x: -50
    })
    .from(".ball__two_1", 0.5, {
      opacity: 0,
      x: -50
    })
    .from(mainInfoIndex, 0.5, {
      delay: -0.5,
      ease: Power2.easeOut,
      y: 30,
      opacity: 0
    });
  // .from(typeTitleIndex, 0.5, {
  //   delay: -0.2,
  //   y: 30,
  //   opacity: 0
  // })
  // .from(titleIndex, 0.5, {
  //   delay: -0.5,
  //   y: 30,
  //   opacity: 0
  // })
  // .from(infoIndex, 0.5, {
  //   delay: -0.5,
  //   y: 30,
  //   opacity: 0
  // });
});
//swiper
//第２個slider
if (window.matchMedia("(max-width:1024px)").matches) {
  const swiper2 = new Swiper(".ise2_swiper-container", {
    slidesPerView: 1,
    navigation: {
      nextEl: ".ise2-next",
      prevEl: ".ise2-prev"
    }
  });
}
const s4slide = function() {
  const t3 = new TimelineMax({});
  t3.from(".data__show", 0.5, {
    opacity: 0,
    y: 10,
    clearProps: "all"
  })
    .from(".info__show>.title", 0.5, {
      opacity: 0,
      y: 10,
      clearProps: "all"
    })
    .from(".info__show>.info", 0.5, {
      opacity: 0,
      y: 10,
      clearProps: "all"
    });
};
//swiper
//swiper
if (window.matchMedia("(max-width:767px)").matches) {
  const swiper4 = new Swiper(".ise4_swiper-container", {
    slidesPerView: 1,
    pagination: {
      el: ".ise4_swiper-pagination",
      clickable: true
    },
    loop: true,
    on: {
      slideChange: s4slide
    }
  });
}
//滾動式差區域
//滾動式差區域
//滾動式差區域
if (window.matchMedia(bkpDsk).matches) {
  let img = document.querySelector(".se3__dsk");
  new simpleParallax(img, {
    scale: 1.2
  });
} else {
  let imgmbl = document.querySelector(".se3__mbl");
  new simpleParallax(imgmbl, {
    scale: 1.2
  });
}

//=========================================動態數字
//=========================================動態數字
const showNum = function() {
  let options = {
    useEasing: true,
    useGrouping: true,
    separator: ",",
    decimal: "."
  };
  let num = $(".title > .num");
  //console.log(num);
  num.each(function(index) {
    let value = $(num[index]).html();
    //console.log(value);
    //抓到網頁上面的數值
    let numAnination = new CountUp(num[index], 0, value, 0, 5, options);
    numAnination.start();
  });
};

//=========================================動態數字
//=========================================動態數字

//=========================================捲軸區域
//=========================================捲軸區域
//=========================================捲軸事件
let controller = new ScrollMagic.Controller({
  //addIndicators: true
});

let scene1 = new ScrollMagic.Scene({
  triggerElement: ".ise2__items",
  triggerHook: 0.85,
  reverse: !1
});
scene1
  .on("enter", function(event) {
    showNum();
  })
  .addTo(controller);

//=========================================捲軸區域
//=========================================捲軸區域
//=========================================捲軸事件

let indexAni1 = new TimelineLite();
indexAni1
  .staggerFrom(
    ".ise2__card",
    0.8,
    {
      y: "+=150",
      opacity: 0,
      clearProps: "opacity, transform"
    },
    0.2
  )
  .from(".ise2__items", 0.5, {
    delay: 0.3,
    y: 30,
    opacity: 0
  });
//indexAni1;
let indexcardScroll = new ScrollMagic.Scene({
  triggerElement: ".index__se2", //觸發點
  triggerHook: 0.75,
  reverse: !1
})
  .setTween(indexAni1)
  .addTo(controller);

//=========================================捲軸區域
//=========================================捲軸區域
//=========================================捲軸事件

let indexAni2 = new TimelineLite();
indexAni2
  .from(".s3title", 1, {
    delay: 1,
    y: 30,
    opacity: 0,
    clearProps: "opacity, transform"
  })
  .from(".s3_secTitle", 1, {
    y: 30,
    opacity: 0,
    delay: -0.8,
    clearProps: "opacity, transform"
  })
  .from(".s3_info", 1, {
    y: 30,
    opacity: 0,
    delay: -0.8,
    clearProps: "opacity, transform"
  });
indexAni2;
let index1Scroll = new ScrollMagic.Scene({
  triggerElement: ".index__se3", //觸發點
  triggerHook: 0.75,
  reverse: !1
})
  .setTween(indexAni2)
  .addTo(controller);

//=========================================捲軸區域
//=========================================捲軸區域
//=========================================捲軸事件

let indexAni3 = new TimelineLite();
indexAni3.staggerFrom(
  ".ise4__card",
  1,
  {
    y: "+=30",
    opacity: 0,
    clearProps: "opacity, transform"
  },
  0.5
);
indexAni3;
let index2Scroll = new ScrollMagic.Scene({
  triggerElement: ".index__se4", //觸發點
  triggerHook: 0.75,
  reverse: !1
})
  .setTween(indexAni3)
  .addTo(controller);

document.addEventListener(
  "DOMContentLoaded",
  e => {
    $(".main__greenLine").hide();
    setTimeout(() => {
      $(".main__greenLine").show();
    }, 2000);
  },
  false
);

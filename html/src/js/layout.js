let em = new Vue({
  el: "#commonFooter",
  data: {
    commonData
  }
});
//==============================================選單按鈕用
//==============================================選單按鈕用
$(document).ready(function() {
  $(".btn__area").on("click", function() {
    $(".btn__area").toggleClass("active");
    $(".layout__list").toggleClass("active");
    $(".common-header").toggleClass("active");
    $(".btn__language").toggleClass("active");
  });
  $(".email").on("click", function() {
    $(".showmsg").addClass("active");
  });

  $(".show__close").on("click", function() {
    $(".showmsg").removeClass("active");
  });
});

// let indexAni = function() {
//   let indexTotalAni = new TimelineLite();
// };

//==============================================回到頂端按鈕用
//==============================================回到頂端按鈕用
//perpos需要引入才可以使用
window.addEventListener("load", () => {
  let topUP = document.querySelector("#upTop");
  topUP.addEventListener(
    "click",
    () => {
      TweenLite.to(window, 2, {
        scrollTo: { y: 0, x: 0, autoKill: false },
        ease: Power4.easeOut
      });
    },
    false
  );
});

document.addEventListener(
  "DOMContentLoaded",
  e => {
    $("html").addClass("active");
    setTimeout(() => {
      $(".full__loading").css({ opacity: "0" });
      $("html").removeClass("active");
    }, 4000);
  },
  false
);

// $(document).ready(function() {
//   $(window).resize(function() {
//     let wdth = $(window).width();
//     console.log(wdth);
//     if (wdth < 1200) {
//       console.log("整理畫面");
//     }
//   });
// });

const maskAni = function() {
  TweenMax.to(".mask-other", 3, {
    delay: 4,
    width: "0%",
    ease: Power2.easeOut
  });
};

let scrollNum;
//鎖定卷軸
let btnHm = document.querySelector(".btn__area");
btnHm.addEventListener(
  "click",
  () => {
    if ($("html").hasClass("active")) {
      $("html").removeClass("active");
      $("body").css({ top: "" });
      $("html,body").scrollTop(scrollNum);
    } else {
      scrollNum = $(window).scrollTop();
      $("body").css({ top: scrollNum * -1 });
      $("html").toggleClass("active");
    }
  },
  false
);
